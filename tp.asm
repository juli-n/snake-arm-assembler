.data
 /* Definicion de datos */
mapa: .asciz "+------------------------------------------------+\n|                ****************                |\n|                *** VIBORITA ***                |\n|                ****************                |\n+------------------------------------------------+\n|     M                                          |\n|          M                                M    |\n|                    @***                        |\n|                     M                          |\n|                                                |\n|                                                |\n|                                                |\n|                                                |\n|                                                |\n+------------------------------------------------+\n| Puntaje: 0                    Nivel:           |\n+------------------------------------------------+\n" @ \n enter
longitud = . - mapa

cls: .asciz "\x1b[H\x1b[2J" /*una manera de borrar la pantalla usando ansi escape
codes*/
lencls = .-cls @ Borra la pantalla :)

condicion: .word 0	@0 simula ser false, 1 simula ser true
go: .ascii "GAME OVER\n"
gameover: .word 0	@0 simula ser false,
			@1 simula ser true
long: .word 4		@longitud de la vibora
vibora: .word 378
cola: .word 381
mensaje: .ascii "Ingrese un caracter valido:"
cadena:	.asciz " "

.text @ Definición de código del programa

printPant:
	.fnstart
	push { lr }
        mov r7, #4      /*Salidad por pantalla*/
        mov r0, #1      /*indicamos a swi que sera uan cadena*/
        ldr r2, =longitud       /*Tamaño de la cadena*/
        ldr r1, =mapa           /*cargamos en r1 la direccion del mensaje*/
        swi 0                   /*SWI software interrupt*/

        @Salida por pantalla
        mov r7, #4              @Salida por pantalla
        mov r0, #1              @Salida de cadena
        mov r2, #28             @Tamaño de la cadena
        ldr r1, =mensaje        @cargo en r1, la direccion del mensaje a mostrar
        swi 0

        @Lectura de teclado
        mov r7, #3              @Lectura de teclado
        mov r0, #0              @Ingreso de cadena
        mov r2, #1              @Leer caracteres
        ldr r1, =cadena         @donde se guarda la cadena ingresada
        swi 0
	pop { lr }
	bx lr
	.fnend


/*limpia la pantalla*/
limpiar:
	.fnstart
	push {lr}
	mov r0, #1
	ldr r1, =cls
	ldr r2, =lencls
	mov r7, #4
	swi #0
	pop {lr}
	bx lr
	.fnend

/*imprimimos porpantalla game over*/
imprimirGameOver:
	.fnstart
	push {lr}
	mov r7, #4
	mov r0, #1
	mov r2, #11
	ldr r1, =go
	swi 0
	bx lr
	pop {lr}
	.fnend

/*
Pregunta si hay una pared
*/
hayPared:
	.fnstart
	push {lr}
	ldrb r7, [r9, r6]
	cmp r7, #'-'
	beq cambiaNum
	cmp r7, #'|'
	beq cambiaNum
	cmp r7, #'*'
	bal sali
	cambiaNum:
		ldr r5, =gameover
		ldr r6, [r5]
		add r8, r6, #1
		str r8, [r5]
	sali:
		pop {lr}
		bx lr
		.fnend

/*
pregunta si hay una manzana el la posicion que le pasamos
en este caso r6
*/
hayManzana:
	.fnstart
	push {lr}
	ldrb r7, [r9, r6]
	cmp r7, #'M'
	bne salir
	bl comerManzana
	bl actualizarPuntaje
salir:
	pop {lr}
	bx lr
	.fnend

/*
si en la nueva posicion hay una manzana suma 1 a la condicion
*/
comerManzana:
	.fnstart
	push {lr}
	add r12, r12, #1
	str r12, [r10]
	pop {lr}
	bx lr
	.fnend

/*
la posicion es fija, entonces solamente se cambiara el digito en esa posicion
*/
actualizarPuntaje:
	.fnstart
	push {r7, lr}
	ldr r9, =mapa
	ldrb r7, [r9, #776]
	add r7, r7, #1
	strb r7, [r9, #776]
	pop {r7, lr}
	bx lr
	.fnend

/*

*/
nuevaPosicionCola:
	.fnstart
	push {lr}
	mov r11, #' '

	sub r2, r2, #1
	ldrb r10, [r9, r2]
	cmp r10, #'*'
	beq newTailIzq

	sub r2, r2, #50
	ldrb r10, [r9, r2]
	cmp r10, #'*'
	beq newTailArr

	add r2, r2, #52
	ldrb r10, [r9, r2]
	cmp r10, #'*'
	beq newTailDer

	add r2, r2, #50
	ldrb r10, [r9, r2]
	cmp r10, #'*'
	beq newTailAbj
	bal salirFuncion
	newTailIzq:
		add r2, r2, #1
		strb r11, [r9, r2]
		sub r2, r2, #1
		strb r2, [r1]
		bal salirFuncion
	newTailArr:
		add r2, r2, #51
		strb r11, [r9, r2]
		sub r2, r2, #51
		strb r2, [r1]
		bal salirFuncion
	newTailDer:
		sub r2, r2, #1
		strb r11, [r9,r2]
		add r2, r2, #1
		strb r2, [r1]
		bal salirFuncion
	newTailAbj:
		sub r2, r2, #51
		strb r11, [r9, r2]
		add r2, r2, #51
		strb r2, [r1]
		bal salirFuncion
	salirFuncion:
	pop {lr}
	bx lr
	.fnend

/*
carga en el registro desplazado la "cabeza" de la serpiente
recibe r6
*/
avanzarNuevaPosicionVibora:
	.fnstart
	push {lr}
	mov r8, #'@'
	strb r8, [r9, r6]
	pop {lr}
	bx lr
	.fnend
/*
recibe la tecla del usuario
realiza los cambios en la fila o columna dependiendo en que direccion se mueva
si el usuario presiona:
q para salir completamente del juego
w para que la vibora se mueva hacia arriba
a para que se mueva para la izquierda
s para que se mueva hacia abajo
d para que la vibora se mueva a la derecha
*/

leeTecla:
	.fnstart
	push {lr}
	ldr r0, =cadena
	ldr r4, [r0]
	ldr r9, =mapa
	ldr r1, =cola
	ldr r2, [r1]
	ldr r5, =vibora
	ldr r6, [r5]
	ldr r10, =condicion
	ldr r12, [r10]

	cmp r4, #'q'
	beq salirjuego
	cmp r4, #'w'
	beq arriba
	cmp r4, #'a'
	beq izquierda
	cmp r4, #'s'
	beq abajo
	cmp r4, #'d'
	beq derecha
	bl salirFun

	salirjuego:
		ldr r1, =gameover
		ldr r2, [r1]
		add r2, r2, #1
		str r2, [r1]
		bl salirFun
	izquierda:
		mov r11, #'*'
		strb r11, [r9 ,r6]
		sub r6, r6, #1 @cantidadColu, le resta 1
		str r6, [r5]	@r5<-r6
		bl hayManzana
		bl hayPared
		bl avanzarNuevaPosicionVibora
		cmp r12, #0
		bne condicionIzq
		bl nuevaPosicionCola
		bal salirIzq
		condicionIzq:
			mov r12, #0
			str r12, [r10]
		salirIzq:
			bl salirFun
	derecha:
		mov r11, #'*'
		strb r11, [r9, r6]
		add r6, r6, #1
		str r6, [r5]
		bl hayManzana
		bl hayPared
		bl avanzarNuevaPosicionVibora
		cmp r12, #0
		bne condicionDer
		bl nuevaPosicionCola
		bal salirDer
		condicionDer:
			mov r12, #0
			str r12, [r10]
		salirDer:
			bl salirFun
	arriba:
		mov r11, #'*'
                strb r11, [r9, r6]
                sub r6, r6, #51
                str r6, [r5]
		bl hayManzana
		bl hayPared
		bl avanzarNuevaPosicionVibora
		cmp r12, #0
		bne condicionArr
		bl nuevaPosicionCola
		bal salirArr
		condicionArr:
			mov r12, #0
			str r12, [r10]
		salirArr:
			bl salirFun
	abajo:
		mov r11, #'*'
                strb r11, [r9, r6]
                add r6, r6, #51
                str r6, [r5]
		bl hayManzana
		bl hayPared
		bl avanzarNuevaPosicionVibora
		cmp r12, #0
		bne condicionAbj
		bl nuevaPosicionCola
		bal salirAbj
		condicionAbj:
			mov r12, #0
			str r12, [r10]
		salirAbj:
			bl salirFun
	salirFun:
		pop {lr}
		bx lr
		.fnend
jugar:
	.fnstart
	push {lr}
	ciclo:
		ldr r0, =gameover
		ldr r1, [r0]
		cmp r1, #1
		beq cierraJuego
		bl leeTecla
		bl printPant
		bl limpiar
		bal ciclo
	cierraJuego:
		bl limpiar
		bl imprimirGameOver
		pop {lr}
		bx lr
		.fnend

.global main @ global, visible en todo el programa

main:

	bl jugar


fin:
	mov r7, #1 /* Salida al sistema*/
	swi 0
